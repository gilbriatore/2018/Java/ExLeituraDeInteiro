
import java.util.*;

public class Programa {
	
	public static void main(String[] args) {
		
		//Entrada de dados;
		Scanner leitor = new Scanner(System.in);
		System.out.println("Digite o valor 'a': ");
		int a = leitor.nextInt();	
		
		System.out.println("Digite o valor 'b': ");
		int b = leitor.nextInt();
		
		//Processamento de dados;
		//int a = 10; //hardcoded (valor fixo)
		//int b = 5;  //hardcoded (valor fixo)
		//int c = a + b;
	
		//Sa�da de dados;
		//Concatena��o (string + n�mero)
		System.out.println("Total: " + (a + b));
		
		leitor.close();
		
	}
}
